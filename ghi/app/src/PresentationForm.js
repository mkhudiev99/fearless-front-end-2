import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: '',
            companyName: '',
            presenterEmail: '',
            title: '',
            // statuses: [],
            // status: '',
            conferences: [],
            conference: '',


        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }


    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.conferences;
        // delete data.statuses
        delete data.presenterName;
        delete data.companyName;
        delete data.presenterEmail
        console.log(data);

        const selectTag = document.getElementById('conference');
        const conferenceId = selectTag.options[selectTag.selectedIndex].value;
        const presentationUrl = `http://localhost:8000/${conferenceId}presentations/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // const presentationTag = document.getElementById('presentation');
        // const presentationId = selectTag.options[presentationTag.selectedIndex].value;
        // const statusTag = document.getElementsByName('status');
        // const statusName = selectTag.options[statusTag.selectedIndex].value;
        // const statusUrl = `http://localhost:8000/api/presentations/${presentationId}/${statusName}/`;
        // const fetchOptions2 = {
        //     method: 'post',
        //     body: JSON.stringify(data),
        //     headers: {
        //         'Content-Type': 'application/json',
        //     },
        // };

        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
            const cleared = {
                presenterName: '',
                companyName: '',
                presenterEmail: '',
                title: '',
                // status: '',
                conference: '',
               };
               this.setState(cleared);
        }
    }


render() {
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value = {this.state.presenterName} placeholder="Presenter name" required type="text" name="presenterName" id="presenter_name" className="form-control"/>
                                <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value = {this.state.companyName} placeholder="Company name" required type="text" name="companyName" id="company_name" className="form-control"/>
                                <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value = {this.state.presenterEmail} placeholder="Presenter email" required type="text" name="presenterEmail" id="presenter_email" className="form-control"/>
                                <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value = {this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                                <label htmlFor="title">Title</label>
                        </div>

                        {/* <div class="mb-3">
                        <select onChange={this.handleChange} value = {this.state.status} required id="status" name="status" class="form-select">
                                <option selected value="">Choose a status</option>
                                {this.state.statuses.map(status => {
                                        return (
                                            <option key={status.id} value={status.id}>
                                                {status.name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div> */}
                        <div className="mb-3">
                            <select onChange={this.handleChange} value = {this.state.conference} required id="conference" name="conference" className="form-select">
                                <option selected value="">Choose a conference</option>
                                {this.state.conferences.map(conference => {
                                        return (
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )}
}

export default PresentationForm;