import React, { useState, useEffect } from 'react'

const AttendeesList = () => {
    const [attendees, setAttendees] = useState([])


    const getAttendees = async () => {
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const response = await fetch(attendeeUrl);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAttendees(data);
        }

    }
    useEffect(() => {
        getAttendees()
    }, [])


  return (
    <div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th scope="col">href</th>
                <th scope="col">Email</th>
                <th scope="col">Name</th>
                <th scope="col">Company Name</th>
                <th scope="col">Created</th>
                <th scope="col">Conference</th>
            </tr>
        </thead>
        <tbody>
            {attendees.attendees?.map(attendee => {
                    return (
                        <tr key={attendee.href}>
                            <td>{attendee.email}</td>
                            <td>{attendee.name}</td>
                            <td>{attendee.company_name}</td>
                            <td>{attendee.created}</td>
                            <td>{attendee.conference}</td>
                        </tr>
            )})}


        </tbody>
    </table>
</div>
)
}

export default AttendeesList