import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            location: '',
            locations: [],
          };
        this.handleChange = this.handleChange.bind(this);

        // this.handleNameChange = this.handleNameChange.bind(this);
        // this.handleStartsChange = this.handleStartsChange.bind(this);
        // this.handleEndsChange = this.handleEndsChange.bind(this);
        // this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        // this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        // this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        // this.handleLocationChange = this.handleLocationChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
      }
    


    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.max_attendees = data.maxAttendees;
        delete data.maxAttendees;
        data.max_presentations= data.maxPresentations;
        delete data.maxPresentations;
        delete data.locations;
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConf = await response.json();
            console.log(newConf);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
              };
              this.setState(cleared);


        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({ starts: value })
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ ends: value })
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({ description: value })
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({ maxPresentations: value })
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({ maxAttendees: value })
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }
    

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                                <label htmlFor="room_count">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="room_count">Ends</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.description} placeholder="Description" required type="text" name="description" id="description" className="form-control" />
                                <label htmlFor="city">Description</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.maxPresentations} placeholder="Max presentations" required type="number" name="maxPresentations" id="max_presentations" className="form-control" />
                                <label htmlFor="city">Max presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.maxAttendees} placeholder="Max attendees" required type="number" name="maxAttendees" id="max_attendees" className="form-control" />
                                <label htmlFor="city">Max attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value = {this.state.location} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default ConferenceForm;
