import React from 'react'
import Nav from './Nav';
import logo from './logo.svg';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm'
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage'



function App() {
  // if (props.attendees === undefined) {
  //   return null;
  // }
  return (
    
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
          <Route path="" element={<AttendeesList/>} />
          <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />
        </Routes>
        {/* </div> */}
      </BrowserRouter>
    
  );
}

export default App;









// function App(props) {
//    if (props.attendees === undefined) {
//    return null;
//    }
//   return (
//     <>
//     <Nav />
//     <div className="container">
//       <table className="table table-striped">
//       {/* <table className="table table-dark table-striped"> */}
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>

//           {/* for (let attendee of props.attendees) {
//               <tr>
//                 <td>{ attendee.name }</td>
//                 <td>{ attendee.conference }</td>
//               </tr>
//             } */}
//           {props.attendees.map(attendee => {
//             return (
//               <tr key = {attendee.href}>
//                 <td>{attendee.name}</td>
//                 <td>{attendee.conference}</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     </div>
//     </>
//   );
// }

// export default App;






// function App(props) {
// if (props.attendees === undefined) {
//   return null;
// }
//   return (
//     <div>
//     Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;






{/* <div className="App"> */ }
{/* <header className="App-header">
<img src={logo} className="App-logo" alt="logo" />
<p>
  Edit <code>src/App.js</code> and save to reload.
</p>
<a
  className="App-link"
  href="https://reactjs.org"
  target="_blank"
  rel="noopener noreferrer"
>
  Learn React
</a>
</header> */}