import { NavLink } from 'react-router-dom';
function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Navbar</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">Home</a>
              </li>
              
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/locations/new/">Create New Location</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/conferences/new/">Create New Conference</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/attendees">Attendees List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/attendees/new/">Create New Attendee</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/presentations/new/">Create New Presentation</NavLink></li>
                </ul>
            </ul>
  
          </div>
        </div>
      </nav>
    )
  }



  
//         <header>
//             <nav className="navbar navbar-expand-lg bg-light">
//                 <div className="container-fluid">
//                     <a className="navbar-brand" href="#">Conference GO</a>
//                     <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
//                         <span className="navbar-toggler-icon"></span>
//                     </button>
//                     <div className="collapse navbar-collapse" id="navbarNav">
//                         <ul className="navbar-nav">
//                             <li className="nav-item">
//                                 <a className="nav-link active" aria-current="page" href="#">Home</a>
//                             </li>
//                             <li className="nav-item">
//                                 <a className="nav-link" id="create-location-form" aria-current="page" href="new-location.html">New location</a>
//                             </li>
//                             <li class="nav-item">
//                                 <a className="nav-link active" id="create-attendee-form" aria-current="page" href="new-attendee.html">New Attendee</a>
//                             </li>
//                             <li className="nav-item">
//                                 <a className="nav-link" id="create-conference-form" aria-current="page" href="new-conference.html">New Conference</a>
//                             </li>
//                             <li className="nav-item">
//                                 <a className="nav-link" id="create-presentation-form" aria-current="page" href="new-presentation.html">New Presentation</a>
//                             </li>
//                         </ul>
//                     </div>
//                 </div>
//             </nav>
//         </header>
//     );
// }


export default Nav;