

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            const selectTag = document.getElementById('conference');
            for (let conference of data.conferences){
                const option = document.createElement('option')
                option.value = conference.href.split('/')[3]
                option.innerHTML = conference.name
                selectTag.appendChild(option)
            }

        }
            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async (event) => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                console.log(json);

                const confID = Object.fromEntries(formData)
                console.log(confID);
                
                const presentationUrl = `http://localhost:8000/api/conferences/${confID.conference}/presentations/`;
                
                const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            
                };
                const response = await fetch(presentationUrl, fetchConfig);
                if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
                }
        })})



    //     const formTag = document.getElementById('create-presentation-form');
    //     formTag.addEventListener('submit', async (event) => {
    //         submitForm(event)
    //         event.preventDefault();
    //         const formData = new FormData(formTag);
    //         const json = JSON.stringify(Object.fromEntries(formData));
    //         console.log(json);

       
    //         const presentationUrl = 'http://localhost:8000/api/conferences/${option.value}/presentations/';
            
    //         const fetchConfig = {
    //         method: "post",
    //         body: json,
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //         };
    //         const response = await fetch(presentationUrl, fetchConfig);
    //         if (response.ok) {
    //         formTag.reset();
    //         const newPresentation = await response.json();
    //         console.log(newPresentation);
    //         }
    //          })
    // }})