function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="card-group">
    <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${location}</p>
          <p class="card-text">${description}</p>
          
        </div>
        <div class="card-footer text-muted">${start} - ${end}</div>
      </div>
      </div>
    `;
  }




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {

            // console.log(response);

        } else {

            const data = await response.json();
            let count = 0
            
            for (let conference of data.conferences) {

            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);


                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts);
                    const end = new Date(details.conference.ends);
                    const startss = start.toLocaleDateString() ;
                    const endss = end.toLocaleDateString() ;
                    const location = details.conference.location.name
                    const html = createCard(name, description, pictureUrl, startss, endss, location);
                    const column = document.querySelector(`.col.num${count % 3}`);
                    column.innerHTML += html;
                    count++

              }
            }

            
        }

        } catch (e) {
            console.error(e)
        }

    
    ;

  });

